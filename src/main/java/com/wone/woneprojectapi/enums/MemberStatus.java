package com.wone.woneprojectapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberStatus {
    NORMAL("일반"),
    AWAY("탈퇴");

    private final String statusType;
}
