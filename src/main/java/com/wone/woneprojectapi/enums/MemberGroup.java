package com.wone.woneprojectapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberGroup {
    USER_MEMBER("사용자"),
    ADMIN("관리자");

    private final String partType;
}
