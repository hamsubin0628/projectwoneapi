package com.wone.woneprojectapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CategoryType {
    SEND_MONEY("https://storage.googleapis.com/cdn-wone/01_send_money.png", "송금"),
    STUDY("https://storage.googleapis.com/cdn-wone/02_study.png", "교육"),
    MEAL("https://storage.googleapis.com/cdn-wone/03_meal.png", "식사"),
    COFFEE("https://storage.googleapis.com/cdn-wone/04_cafe.png", "커피구매"),
    TRANSPORT("https://storage.googleapis.com/cdn-wone/05_bus.png", "대중교통"),
    SHOPPING("https://storage.googleapis.com/cdn-wone/06_shopping.png", "쇼핑"),
    BILL("https://storage.googleapis.com/cdn-wone/07_bill.png", "공과금"),
    MEDICAL("https://storage.googleapis.com/cdn-wone/08_hospital.png", "의료비"),
    CULTURAL("https://storage.googleapis.com/cdn-wone/09_culture.png", "문화생활"),
    MEETING("https://storage.googleapis.com/cdn-wone/10_meeting.png", "모임"),
    SUBSCRIPTION("https://storage.googleapis.com/cdn-wone/11_ott.png", "구독료"),
    ETC_PAY("", "기타");

    private final String categoryImgUrl;
    private final String categoryName;
}
