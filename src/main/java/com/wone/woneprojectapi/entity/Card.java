package com.wone.woneprojectapi.entity;

import com.wone.woneprojectapi.enums.CardGroup;
import jakarta.persistence.*;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.CreditCardNumber;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    // LAZY로 변경 필요
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private CardGroup cardGroup;

    @Column(nullable = false, length = 19, unique = true)
    private String cardNumber;

    @Column(nullable = false)
    private LocalDate endDate;

    @Column(nullable = false)
    private Short cvc;

    @Column(length = 50)
    private String etcMemo;
}
