package com.wone.woneprojectapi.model.board.notice;

import com.wone.woneprojectapi.enums.BoardType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BoardNoticeResponse {
    private Long id;
    private BoardType boardType;
    private LocalDate boardCreateDate;
    private String boardTitle;
    private String boardContent;
}
