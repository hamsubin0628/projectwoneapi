package com.wone.woneprojectapi.model.calendar;

import com.wone.woneprojectapi.enums.CategoryType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class CalendarCreateRequest {
    private LocalDate spendDate;
    private String spendTime;
    @Enumerated(value = EnumType.STRING)
    private CategoryType categoryType;
    private Double amount;
}
