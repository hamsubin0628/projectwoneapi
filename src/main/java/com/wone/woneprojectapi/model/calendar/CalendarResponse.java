package com.wone.woneprojectapi.model.calendar;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter

public class CalendarResponse {
    private Long memberId;
    private LocalDate spendDate;
    private String spendTime;
    private String categoryImgUrl;
    private String categoryName;
    private Double amount;
}
