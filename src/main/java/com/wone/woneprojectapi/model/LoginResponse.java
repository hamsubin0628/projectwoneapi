package com.wone.woneprojectapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResponse {
    private Long memberId;
    private String memberName;
}
