package com.wone.woneprojectapi.model.member;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class MemberResponse {
    // 관리자 웹에서는 현재 변수 모두 조회 필요
    // 앱에서는 username, birthdate, password, email, isReceive
    private Long id;
    private String userId;
    private String username;
    private LocalDate birthDate;
    private String password;

    // 관리자 웹 상세보기 화면에서는 시간은 나오지 않아 조회 시에는 LocalDate로 타입 수정?
    private LocalDate joinDate;
    private String memberGroup;
    private String memberStatus;
}
