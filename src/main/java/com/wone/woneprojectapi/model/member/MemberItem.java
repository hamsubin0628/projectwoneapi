package com.wone.woneprojectapi.model.member;

import com.wone.woneprojectapi.enums.MemberGroup;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class MemberItem {
    private Long id;
    private String userId;
    private String username;

    // 관리자 웹 화면에서는 시간은 나오지 않아 조회 시에는 LocalDate로 타입 수정
    private LocalDate joinDate;
    private String memberGroupType;
}
